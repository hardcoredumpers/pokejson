package dpo.pokemon.pokedex;

import com.google.gson.annotations.SerializedName;

public class Sprite {
    @SerializedName("front_default")
    private String frontDefault;

    /**
     * @return la URL de la imatge frontal per defecte del Pokémon
     */
    public String getFrontDefault() {
        return frontDefault;
    }

    /**
     * @param frontDefault la URL de la imatge frontal per defecte del Pokémon
     */
    public void setFrontDefault(String frontDefault) {
        this.frontDefault = frontDefault;
    }
}
