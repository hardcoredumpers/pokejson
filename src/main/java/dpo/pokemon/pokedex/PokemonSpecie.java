package dpo.pokemon.pokedex;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokemonSpecie {
    @SerializedName("flavor_text_entries")
    private List<FlavorText> flavorTexts;

    /**
     * @return llista de les descripcions del Pokémon en diferents idiomes
     */
    public List<FlavorText> getFlavorTexts() {
        return flavorTexts;
    }

    /**
     * Agafa la descripció del Pokémon segons el idioma
     * @param name idioma de la descripció
     * @return la descripció del Pokémon
     */
    public String getFlavorText(String name) {
        String flavorText = "";
        boolean found = false;
        int length = flavorTexts.size();
        for(int i = 0; i < length && !found; i++) {
            if (flavorTexts.get(i).getLanguage().getName().equals(name)) {
                flavorText = flavorTexts.get(i).getFlavorText();
                found = true;
            }
        }
        return flavorText;
    }
}
