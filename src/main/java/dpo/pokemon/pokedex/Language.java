package dpo.pokemon.pokedex;

import com.google.gson.annotations.SerializedName;

public class Language {
    @SerializedName("name")
    private String name;
    @SerializedName("url")
    private String url;

    /**
     * @return el nom del idioma
     */
    public String getName() {
        return name;
    }

    /**
     * @return la URL del idioma
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param name el nom del idioma
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param url la URL del idioma
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
