package dpo.pokemon.pokedex;

import com.google.gson.annotations.SerializedName;
import dpo.pokemon.pokedex.Language;

public class FlavorText {
    @SerializedName("flavor_text")
    private String flavorText;
    @SerializedName("language")
    private Language language;

    /**
     * @return la descripció del Pokémon
     */
    public String getFlavorText() {
        return flavorText;
    }

    /**
     * @return idioma de la descripció
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * @param flavorText la descripció del Pokémon
     */
    public void setFlavorText(String flavorText) {
        this.flavorText = flavorText;
    }

    /**
     * @param language idioma de la descripció
     */
    public void setLanguage(Language language) {
        this.language = language;
    }
}
