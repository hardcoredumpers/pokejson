package dpo.pokemon.game;

import com.google.gson.annotations.SerializedName;
import dpo.pokemon.pokedex.Sprite;

public class PokemonAux {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("capture_rate")
    private int captureRate;

    // Atributs que venen de dpo.pokemon.structure.Webservice
    @SerializedName("base_experience")
    private int baseExperience;
    @SerializedName("height")
    private int height;
    @SerializedName("weight")
    private int weight;
    @SerializedName("sprites")
    private Sprite sprites;

    /**
     * @return l'identificador del Pokémon
     */
    public int getId() {
        return this.id;
    }

    /**
     * @return el nom del Pokémon
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return la probabilitat de captura del Pokémon
     */
    public int getCaptureRate() {
        return this.captureRate;
    }

    /**
     * @return l'experiència base del Pokémon
     */
    public int getBaseExperience() {
        return baseExperience;
    }

    /**
     * @return l'altura del Pokémon
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return el pes del Pokémon
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @return el Sprites del Pokémon
     */
    public Sprite getSprites() {
        return sprites;
    }

    /**
     * @param id l'identificador del Pokémon
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param name el nom del Pokémon
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param captureRate la probabilitat de captura del Pokémon
     */
    public void setCaptureRate(int captureRate) {
        this.captureRate = captureRate;
    }

    /**
     * @param baseExperience l'experiència base del Pokémon
     */
    public void setBaseExperience(int baseExperience) {
        this.baseExperience = baseExperience;
    }

    /**
     * @param height l'altura del Pokémon
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @param weight el pes del Pokémon
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @param sprites els Sprites del Pokémon
     */
    public void setSprites(Sprite sprites) {
        this.sprites = sprites;
    }
}
