package dpo.pokemon.game;

public class Legendary extends Pokemon {
    private static String kind = "legendary";
    private Gym gym;

    /**
     * @return el gimnàs
     */
    public Gym getGym() {
        return gym;
    }

    /**
     * Càlcula el valor de la captura del Pokémon Llegendari segons de la seva fórmula
     * @param probPokemon la probabilitat de captura del Pokémon
     * @param probBall la probabilitat de captura de la bola
     * @return el valor de la captura calculada
     */
    @Override
    public double calculateCapture(int probPokemon, int probBall) {
        return (Math.pow(probBall, 1.5) / 256) + ((Math.pow(probPokemon, Math.PI))/2048);
    }
}
