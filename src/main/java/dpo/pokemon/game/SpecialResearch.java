package dpo.pokemon.game;

import com.google.gson.annotations.SerializedName;

public class SpecialResearch {
    @SerializedName("name")
    private String name;
    @SerializedName("quests")
    private Quest[] quests;
    private boolean completed;

    /**
     * Constructor de SpecialResearch. Per defecte, no està completada.
     */
    public SpecialResearch() {
        completed = false;
    }

    /**
     * @return el nom de la Recerca Especial
     */
    public String getName() {
        return name;
    }

    /**
     * @param name el nom de la Recerca Especial
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return array de missions que s'han de completar
     */
    public Quest[] getQuests() {
        return quests;
    }

    /**
     * @param quests array de missions que s'han de completar
     */
    public void setQuests(Quest[] quests) {
        this.quests = quests;
    }

    /**
     * @return true si està completada. Altrament, false.
     */
    public boolean isCompleted() {
        return completed;
    }

    /**
     * @param completed flag que indica si la missió està completada
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
