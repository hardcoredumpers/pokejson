package dpo.pokemon.game;

import com.google.gson.annotations.SerializedName;

/**
 *
 */
public class Ball {
    @SerializedName("name")
    private String name;
    @SerializedName("capture_rate")
    private int captureRate;
    @SerializedName ("price")
    private int price;

    /**
     *
     * @return el nom de la ball
     */
    public String getName() {
        return name;
    }

    /**
     * Canvia el nom de la bola
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return retorna el capture rate
     */
    public int getCaptureRate() {
        return captureRate;
    }

    /**
     * Canvia per el nou capture rate
     * @param captureRate
     */
    public void setCaptureRate(int captureRate) {
        this.captureRate = captureRate;
    }

    /**
     *
     * @return retorna el preu
     */
    public int getPrice() {
        return price;
    }

    /**
     * Canvia per el nou preu
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

}
