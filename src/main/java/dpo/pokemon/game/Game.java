package dpo.pokemon.game;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;

import dpo.pokemon.pokedex.PokemonSpecie;
import dpo.pokemon.structure.*;

import static java.util.stream.Collectors.*;

public class Game {
    private GenericPokemon[] pokemons;
    private PokemonAux[] pokemonsAux;
    private Ball[] pokeballs;
    private Legendary[] legendaries;
    private Mythical[] mythicals;
    private Mythical[] mythicalsAux;
    private Player player;

    public Game(Player player) {
        this.player = player;
    }

    // Getters and setters
    /**
     * @return pokemons array de Pokémons de tipus Genèric
     */
    public GenericPokemon[] getPokemons() {
        return pokemons;
    }

    /**
     * @return pokeballs array dels tipus de Pokéball que tenim en el joc
     */
    public Ball[] getPokeballs() {
        return pokeballs;
    }

    /**
     * @return legendaries array de Pokémons de tipus Llegendari
     */
    public Legendary[] getLegendaries() {
        return legendaries;
    }

    /**
     * @return mythicals array de Pokémons de tipus Mític
     */
    public Mythical[] getMythicals() {
        return mythicals;
    }

    /**
     * @return pokemonsAux array de Pokémons auxiliars
     */
    public PokemonAux[] getPokemonsAux() {
        return pokemonsAux;
    }

    /**
     * @return mythicalsAux array de Pokémons Mítics auxiliars
     */
    public Mythical[] getMythicalsAux() {
        return mythicalsAux;
    }

    /**
     * @param pokemons array de Pokémons que existeixen en el joc
     */
    public void setPokemons(GenericPokemon[] pokemons) {
        this.pokemons = pokemons;
    }

    /**
     * @param pokemonsAux array de Pokémons auxiliars
     */
    public void setPokemonsAux(PokemonAux[] pokemonsAux) {
        this.pokemonsAux = pokemonsAux;
    }

    /**
     * @param pokeballs array dels tipus de Pokéballs
     */
    public void setPokeballs(Ball[] pokeballs) {
        this.pokeballs = pokeballs;
    }

    /**
     * @param legendaries array de Pokémons de tipus Llegendari
     */
    public void setLegendaries(Legendary[] legendaries) {
        this.legendaries = legendaries;
    }

    /**
     * @param mythicals array de Pokémons de tipus Mític
     */
    public void setMythicals(Mythical[] mythicals) {
        this.mythicals = mythicals;
    }

    /**
     * Inicialitzar la partida
     */

    /**
     * Inicialitza el joc llegint els fitxers JSON i ho guarda als arrays que pertoca.
     */
    public void initGame() {
        JsonReader jsonReader = new JsonReader();
        this.pokemonsAux = jsonReader.getPokemonsAux();
        this.pokemons = turnIntoPokemons(pokemonsAux);
        this.legendaries = jsonReader.getLegendary();
        this.mythicals = jsonReader.getMythical();
        this.mythicalsAux = jsonReader.getMythical();
        this.pokeballs = jsonReader.getPokeballs();
    }

    /**
     * Converteix els Pokémons auxiliars en Pokémons de tipus Genèric
     * @param pokemonsAux array de Pokémons auxiliars
     * @return un array de Pokémons de tipus Genèrics amb els mateixos valors dels atributs que els Pokémons auxiliars
     */
    public GenericPokemon[] turnIntoPokemons(PokemonAux[] pokemonsAux) {
        int length = pokemonsAux.length;
        GenericPokemon[] pokes = new GenericPokemon[length];
        for (int i = 0; i < length; i++) {
            pokes[i] = new GenericPokemon();
            pokes[i].setId(pokemonsAux[i].getId());
            pokes[i].setName(pokemonsAux[i].getName());
            pokes[i].setCaptureRate(pokemonsAux[i].getCaptureRate());
            pokes[i].setBaseExperience(pokemonsAux[i].getBaseExperience());
            pokes[i].setHeight(pokemonsAux[i].getHeight());
            pokes[i].setWeight(pokemonsAux[i].getWeight());
            pokes[i].setSprites(pokemonsAux[i].getSprites());
        }
        return pokes;
    }

    // Funcionalidades del menu
    /**
     * Gestiona la compra de monedes per al jugador
     * @param player el jugador que vol comprar monedes
     * @return el jugador amb el nombre de monedes actualitzat
     */
    public Player afegirMonedes(Player player) {
        System.out.println("Quantes monedes vol comprar?");
        Scanner scan = new Scanner(System.in);
        int monedes = scan.nextInt();

        player.buyCoins(monedes);
        return player;
    }

    /**
     * Gestiona la búsqueda de Pokémons salvatges
     * @param p el jugador que busca Pokémons salvatges
     * @return Retorna el jugador que busca un Pokémon salvatge.
     * Es modifica la seva llista de Pokémons capturats segons el resultat de la captura.
     */
    public Player findWildPokemon(Player p) {
        Scanner sc = new Scanner(System.in);
        String pokeName;

        int suma = p.getBallsOwned().size();
        if (suma > 0) {
            System.out.println("\nQuin Pokémon vol buscar?");
            pokeName = sc.nextLine();
            Pokemon pokeFound = buscarPokemon(pokeName);
            if (pokeFound == null) {
                System.out.println("\nHo sentim, però aquest Pokémon no existeix (encara).");
            } else {
                if (!isMythicalOrLegendary(pokeFound)) {
                    String name = pokeFound.getName();
                    System.out.println("\nUn " + firstLetterToUpperCase(name) + " salvatge aparegué!");
                    p = procesoCaptura(p,pokeFound);
                }
            }
        }
        else { System.out.println("\nHo sentim, però no té Pokéballs disponibles, pel que no pot buscar Pokémons.");}
        return p;
    }

    /**
     * Busca el Pokémon indicat a l’array de Pokémons obtinguts del fitxer JSON carregat.
     * @param pokeName string que el jugador ha introduït
     * @return el Pokémon trobat si existeix. Altrament, retorna null.
     */
    public GenericPokemon buscarPokemon (String pokeName) {
        int pokeId = -1;
        GenericPokemon pokeFound = null;
        if (isNumber(pokeName)) {
            pokeId = Integer.parseInt(pokeName);
            pokeFound = searchPokemonById(pokeId);
        }
        else {
            pokeName = pokeName.toLowerCase();
            pokeFound = searchPokemonByName(pokeName);
        }
        return pokeFound;
    }

    /**
     * Comprova si el string és un número
     * @param num el string a comprovar si és un número
     * @return true si és un número. Altrament, false.
     */
    public boolean isNumber(String num) {
        boolean isNumber = true;
        int length = num.length();
        int i = 0;
        while (i < length && isNumber) {
            if (num.charAt(i) < '0' || num.charAt(i) > '9') {
                isNumber = false;
            }
            i++;
        }
        return isNumber;
    }

    /**
     * Busca el Pokémon segons el seu id
     * @param pokeId el id del Pokémon salvatge
     * @return el Pokémon salvatge si existeix. Altrament, retorna null.
     */
    public GenericPokemon searchPokemonById(int pokeId) {
        GenericPokemon poke = null;
        for (int i = 0; i < pokemons.length; i++) {
            if (pokemons[i].getId() == pokeId) {
                poke = pokemons[i];
            }
        }
        return poke;
    }

    /**
     * Busca el Pokémon segons el seu nom
     * @param pokeName el nom del Pokémon salvatge
     * @return el Pokémon salvatge si existeix. Altrament, retorna null.
     */
    public GenericPokemon searchPokemonByName(String pokeName) {
        GenericPokemon poke = null;
        for (int i = 0; i < pokemons.length; i++) {
            if (pokemons[i].getName().equals(pokeName)) {
                poke = pokemons[i];
            }
        }
        return poke;
    }

    /**
     * Comprova si el Pokémon salvatge és llegendari o mític.
     * @param pokeFound el Pokémon salvatge trobat
     * @return true si és un Pokémon Mític o un Pokémon Llegendari. Altrament, false.
     */
    public boolean isMythicalOrLegendary(Pokemon pokeFound) {
        boolean mythicalFound = false, legendFound = false;
        // Controlar si és mythical o no
        for (int i = 0; i < mythicals.length && !mythicalFound; i++) {
            if (mythicals[i].getId() == pokeFound.getId()) {
                if (mythicals[i].getSpecialResearch() != null) {
                    mythicalFound = true;
                    System.out.println("\nHo sentim, però aquest Pokémon és mític i no apareix salvatge.");
                }
            }
        }

        // Controlar si és legendari o no
        for (int i = 0; i < legendaries.length && !legendFound; i++) {
            if (legendaries[i].getId() == pokeFound.getId()) {
                if (legendaries[i].getGym() != null) {
                    legendFound = true;
                    System.out.println("\nHo sentim, però aquest Pokémon és legendari i no apareix salvatge.");
                }
            }
        }
        return mythicalFound || legendFound;
    }

    /**
     * Gestiona el procés de la captura del Pokémon
     * @param player el jugador que intenta capturar el Pokémon
     * @param pokemon el Pokémon a capturar
     * @return el jugador que intenta capturar el Pokémon amb les seves dades actualitzades
     * si la captura és exitosa. Altrament, cap de les seves dades es veu alterada.
     */
    public Player procesoCaptura(Player player, Pokemon pokemon) {
        Scanner sc = new Scanner(System.in);
        Capture capture = new Capture(pokemon, player);
        Ball ball;
        GenericPokemon gp = new GenericPokemon();
        String ballName;

        //Comprovar si li queden Pokéballs
        int intents = capture.getNumTries();
        int suma = player.getBallsOwned().size();
        boolean captured = false;

        //Mentre tingui Pokéballs i encara li queden intents per capturar i mentre no hagi capturat res, es fa aquest bucle
        do {
            if (suma > 0) {
                System.out.println("\nQueden " + suma + " Pokéballs i " + intents + "/5 intents. Quin tipus de Pokéball vol fer servir?");
                ballName = sc.nextLine().toLowerCase();
                ball = getTheBall(ballName);

                //Comprovem que existeix el tipus de pokeball que ha escrit
                if (ball == null || ball.getName() == null) {
                    //Si no existeix la ball, diem que no existeix
                    do {
                        System.out.println("\nAquest tipus no existeix. Quin tipus de Pokéball vol fer servir?");
                        ballName = sc.nextLine().toLowerCase();
                        ball = getTheBall(ballName);
                    } while (ball.getName() == null);
                }
                //Comprovem que encara li queden d'aquell tipus
                if (player.hiHaPokeball(ball.getName())) {
                    player.minusPokeballs(ball.getName());
                    suma--;
                    double formulaResult = gp.calculateCapture(pokemon.getCaptureRate(), ball.getCaptureRate());
                    double randomValue = Math.random();
                    if (formulaResult > randomValue) {
                        captured = player.addCapturedPokemon(pokemon);
                        checkSpecialResearches(pokemon.getId(), player);
                    } else {
                        System.out.println("\nLa " + firstLetterToUpperCase(ball.getName()) + " ha fallat!");
                    }
                    intents--;
                }
                else {
                    System.out.println("\nNo tens d'aquest tipus de Pokéball.");
                }
            }
            else {
                System.out.println("\nNo queden Pokéballs...");
            }
        } while (intents != 0 && suma != 0 && !captured);
        if (suma == 0) {   System.out.println("\nNo queden Pokéballs..."); }
        if (intents == 0) {System.out.println("\nNo queden més intents..."); }
        return  player;
    }

    /**
     * Decrementa la quantitat necessari per completar un quest.
     * @param mitic el Pokémon Mític que ha de capturar en la missió
     * @param index l'índex del Pokémon Mític a l'array de Pokémons de tipus Mític
     */
    private void decrementQuantity (Mythical mitic, int index) {
        // Si la quantitat ja és igual a 0, no decrementem res
        if (mitic.getSpecialResearch().getQuests()[index].getQuantity() != 0){
            mitic.getSpecialResearch().getQuests()[index].decrementQuantity();
        }
    }


    /**
     * Comprova si el Pokémon capturat compta per alguna de les missions de les Recerques Especials
     * @param id el identificador del Pokémon capturat
     * @param player el jugador
     */
    public void checkSpecialResearches(int id, Player player) {
        int lengthMythical = this.mythicalsAux.length;
        boolean found = false, done = false;

        //Per cada recerca
        for (int i = 0; i < lengthMythical; i++) {
            int lengthQuests = this.mythicalsAux[i].getSpecialResearch().getQuests().length;
            found = false;
            done = false;

            //Mirem la llista de quests per veure si es troba l'id
            for (int j = 0; j < lengthQuests && !found; j++) {
                if (this.mythicalsAux[i].getSpecialResearch().getQuests()[j].getTarget() == id) {
                    //Comprovem que no estigui ja a 0
                    found = true;
                    decrementQuantity(this.mythicalsAux[i],j);
                }

                //Si l'hem trobat, mirem si aquesta missió ja està completada
                if (found) {
                    done = checkQuestMissions(this.mythicalsAux[i].getSpecialResearch().getQuests());
                    if (done) {
                        System.out.println("\nMissió de captura completada!");
                        Pokemon pokeFound = searchPokemonById(this.mythicalsAux[i].getId());

                        System.out.println("Recerca Especial completada: Se t'apareix el mític " + firstLetterToUpperCase(pokeFound.getName()) + "!");
                        this.mythicalsAux[i].getSpecialResearch().setCompleted(true);
                        //Procés de captura d'aquest Pokémon
                        //player = procesoCaptura(player,pokeFound);
                        done = player.addCapturedPokemon(pokeFound);

                    }
                }
            }
        }
    }

    /**
     * Comprova que les missions de la Recerca Especial s'hagin completat.
     * @param quest array de missions de la Recerca Especial
     * @return true si s'han completat totes. Altrament, false.
     */
    public boolean checkQuestMissions(Quest[] quest) {
        boolean done = false;
        int suma = 0;
        for (Quest value : quest) {
            if (value.getQuantity() == 0) //si ha completado la quest
            { value.setCompleted(true); }
            suma += value.getQuantity();
        }
        if (suma == 0) { done = true; }
        return done;
    }

    /**
     * Mostrar les Recerques Especials i les missions pendents a completar
     */
    public void checkActualSpecialResearches () {
        // Només mostrem les pendents a completar
        // Si està complert, no mostrem la missió. Si la Recerca Especial està completada, tampoc es mostra
        DecimalFormat df2 = new DecimalFormat("##.##");
        System.out.println("\nRecerques Especials:");

        //Mirem si la Recerca Especial està completada
        for (int i = 0; i < mythicalsAux.length; i++) {
            int id = mythicalsAux[i].getId();
            //Si retorna false, no ha donat 0. Llavors, encara té missions pendents
            if (!checkQuestMissions(mythicalsAux[i].getSpecialResearch().getQuests())) {
                System.out.println("\t-" + mythicalsAux[i].getSpecialResearch().getName() + "(" + firstLetterToUpperCase(pokemonsAux[id - 1].getName()) + "):");
                //Només mostrem les missions que falten per completar
                for (int j = 0; j < mythicalsAux[i].getSpecialResearch().getQuests().length; j++) {
                    int idTarget = mythicalsAux[i].getSpecialResearch().getQuests()[j].getTarget();
                    int tries = getLeftTries(i, j);
                    int left = mythicals[i].getSpecialResearch().getQuests()[j].getQuantity();

                    double percent = (float) (tries) / left * 100;
                    if ((tries - left) != 0) {
                        System.out.print("\t\t* Capturar " + firstLetterToUpperCase(pokemons[idTarget -1].getName()) + ": ");
                        System.out.println(tries + "/" + left + "(" + df2.format(percent) + "%)");
                    }

                }
            }
        }
    }

    /**
     * Calcula quantes captures li queda per completar la missió
     * @param indexMythical l'índex del Pokémon Mític a l'array de Pokémons de tipus Mític
     * @param indexQuest l'índex de la missió
     * @return el valor restant a capturar un Pokémon per poder completar una missió.
     */
    public int getLeftTries (int indexMythical, int indexQuest) {
        int original = mythicals[indexMythical].getSpecialResearch().getQuests()[indexQuest].getQuantity();
        int have = mythicalsAux[indexMythical].getSpecialResearch().getQuests()[indexQuest].getQuantity();
        return original - have;
    }

    /**
     * Gestiona el raid d'un gimnàs
     * @param player el jugador que fa el raid
     * @return el jugador amb les dades actualitzades segons el resultat del raid. Si s'ha capturat el Pokémon Llegendari,
     * tindrà un Pokémon més. En cas contrari, cap de les seves dades es veu alterada.
     */
    public Player raidGym(Player player) {
        Scanner sc = new Scanner(System.in);
        Location playerLoc = new Location();

        // Demanem la ubicació del jugador
        System.out.println("Latitud actual?");
        double lat = sc.nextDouble();
        System.out.println("Longitud actual?");
        double lon = sc.nextDouble();

        // Fixem les coordenades del jugador a un objecte de tipus Location
        playerLoc.setX(lon);
        playerLoc.setY(lat);

        // Comprovem si la ubicació és vàlida
        if (playerLoc.isValid()) {
            player = getDistance(playerLoc, player);
        } else {
            System.out.println("\nLa ubicació introduïda no és vàlida.");
        }
        return player;
    }

    /**
     * Busca el gimnàs més proper al jugador i comença el procés de la captura del Pokémon Llegendari
     * @param playerLoc la ubicació del jugador
     * @param player el jugador que fa el raid
     * @return el jugador amb les dades actualitzades si s'ha capturat el Pokémon Llegendari. Cap de les seves dades es
     * veu alterada si no s'ha capturat el Pokémon Llegendari.
     */
    public Player getDistance(Location playerLoc, Player player) {
        HashMap<Legendary, Double> map = new HashMap<Legendary, Double>();
        List<Double> distance = new ArrayList<Double>();
        double d;

        // Calculem la distància per cada gimnàs
        for (Legendary value : legendaries) {
            d = playerLoc.calculateDistanceTo(value.getGym().getLocation());
            distance.add(d);
            map.put(value,d);
        }

        // Ordenem el map per trobar el gimnàs més proper
        Map<Legendary,Double> sorted = map.entrySet().stream().sorted((Map.Entry.comparingByValue())).collect(toMap(e->e.getKey(),e->e.getValue(),(e1,e2)->e2,LinkedHashMap::new));
        Legendary leg = (Legendary) sorted.keySet().toArray()[0];

        // Agafar el nom del gimnàs
        String name = leg.getGym().getName();
        System.out.println("\nGimnàs més proper: " + name + ". Començant raid...");

        // Agafar el nom del Pokémon en el gimnàs
        String pokename = this.pokemonsAux[leg.getId()-1].getName();
        System.out.println("\nEl boss de raid " + firstLetterToUpperCase(pokename)  + " us repta!");

        // Buscar la informació del Pokémon del gimnàs i comencem la captura
        Pokemon pokeFound = buscarPokemon(this.pokemonsAux[leg.getId()-1].getName());
        player = procesoCaptura(player, pokeFound);

        return player;
    }

    /**
     * Converteix la primera lletra d’un string en majúscula.
     * @param str el string a tractar
     * @return el mateix string però amb la primera lletra en majúscula
     */
    private String firstLetterToUpperCase(String str) {
        String out = str.substring(0,1).toUpperCase() + str.substring(1);
        return out;
    }

    /**
     * Busca el ball en la llista de boles carregada per al joc.
     * @param name el nom de la bola
     * @return l’objecte Ball amb les dades de la bola.
     */
    public Ball getTheBall(String name) {
        for(int i = 0; i < pokeballs.length; i++) {
            if (name.equals(pokeballs[i].getName())) {
                return pokeballs[i];
            }
        }
        return null;
    }


    /**
     *  Busca el gimnàs més proper al jugador
     *  i comença el procés de la captura del Pokémon Llegendari.
     * @throws FileNotFoundException
     */
    public void getPokemonInformation() throws FileNotFoundException{
        Scanner scan = new Scanner(System.in);
        Webservice ws = new Webservice();
        JsonReader jr = new JsonReader();
        String pokeName;

        // Demanem el nom o id del Pokémon
        System.out.println("\nQuin Pokémon vol buscar?");
        pokeName = scan.nextLine();

        // Busquem si existeix el Pokémon
        Pokemon found = buscarPokemon(pokeName);
        if (found != null) {
            if (!isNumber(pokeName)) {
                pokeName = pokeName.toLowerCase();
            }

            // Creem les url on volem agafar les dades del Pokémon
            String url = "https://pokeapi.co/api/v2/pokemon/" + pokeName +"/";
            String urlSpecie = "https://pokeapi.co/api/v2/pokemon-species/" + pokeName +"/";

            // Des del webservice, agafem els JSON strings
            String myJson = ws.getJsonString(url);
            String myJsonSpecie = ws.getJsonString(urlSpecie);

            // Convertim els strings en objectes
            PokemonAux pokeAux = jr.getPokemonForDetails (myJson);
            PokemonSpecie pokeSpecie = jr.getPokemonSpecie(myJsonSpecie);

            // Generem el fitxer HTML on es mostra la informació sobre el Pokémon
            ws.generateHTML(pokeAux, pokeSpecie);
        }
    }


    /**
     * Executa les funcionalitats del joc segons l’opció triada pel jugador.
     */
    public void letTheGameBegin (String option) throws FileNotFoundException {
        switch (Integer.parseInt(option)) {
            //L'usuari comença amb 1000 monedes.
            //Ha de demanar el nombre (enter) de monedes a comprar, proposant per pantalla un preu en euros.
            //L'usuari pot acceptar o no l'oferta amb una Y o N.
            //Comprovar que el nombre demanat sigui positiu, sino, mostrar un error i tornar al menú principal
            case 1:
                player = afegirMonedes(player);
                break;
            case 2:
                displayPokeballs();
                player.buyBalls();
                break;
            case 3:
                player.showInventory();
                break;
            case 4:
                player = findWildPokemon(player);
                break;
            case 5:
                player = raidGym(player);
                break;
            case 6:
                checkActualSpecialResearches();
                break;
            case 7:
                player.showCaptured();
                break;
            case 8:
                getPokemonInformation();
                break;
        }
    }

        /**
         * Mostra les Pokéballs carregat al joc perquè el jugador pugui realitzar la compra.
         */
        public void displayPokeballs() {
            int i;
            char letter = 'a';
            System.out.println("\nTeniu " + player.getCoins() + " monedes.\n");
            System.out.println("Pokéballs disponibles:");
            Ball[] balls = getPokeballs();
            for(i = 0; i < balls.length; i++) {
                System.out.println("\t" + letter + ") " + firstLetterToUpperCase(balls[i].getName())
                        + ": \t" + balls[i].getPrice() + " monedes");
                letter++;
            }
            System.out.println();
            System.out.println("\t" + letter + ") Sortir sense comprar");

            System.out.println();
            System.out.println("Esculli una opció: ");
        }

    /**
     * Agafa la quantitat de balls disponibles al joc.
     * @return el nombre de balls disponibles al joc.
     */
        public int totalBalls () {
            return getPokeballs().length;
        }

    }

