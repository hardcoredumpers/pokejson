package dpo.pokemon.game;

public class Quest {
    private int target;
    private int quantity;
    private boolean completed;

    /**
     * @return el Pokémon que s'ha de capturar
     */
    public int getTarget() {
        return target;
    }

    /**
     * @param target el Pokémon que s'ha de capturar
     */
    public void setTarget(int target) {
        this.target = target;
    }

    /**
     * @return la quantitat de Pokémon que ha de capturar
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity la quantitat de Pokémon que ha de capturar
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Decrementa la quantitat de Pokémon a capturar
     */
    public void decrementQuantity() {
        this.quantity--;
    }

    /**
     * @param completed flag que indica si la missió ja s'ha completada
     */
    public void setCompleted (boolean completed) {this.completed = completed;}

    /**
     * @return true si la missió està completada. Altrament, false.
     */
    public boolean getCompleted () { return  completed;}
}
