package dpo.pokemon.game;

import com.google.gson.annotations.SerializedName;

public class Location {
    @SerializedName("latitude")
    private double x;
    @SerializedName("longitude")
    private double y;

    /**
     * @return la latitud de la ubicació
     */
    public double getX() {
        return x;
    }

    /**
     * @return la longitud de la ubicació
     */
    public double getY() {
        return y;
    }

    /**
     * @param x la latitud de la ubicació
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @param y la longitud de la ubicació
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Comprova si la ubicació és vàlid
     * @return true si és vàlid. Altrament, false.
     */
    public boolean isValid() {
        return (this.getY() >= -90 && this.getY() <= 90 && this.getX() >= -180 && this.getX() <= 180);
    }

    /**
     * Calcula la distància a la ubicació mitjançant la fórmula Haversine
     * @param dest la ubicació de destí
     * @return la distància
     */
    public double calculateDistanceTo(Location dest) {
        final int radiusEarth = 6371; // in miles
        double distance = 0;
        double distLongitud = Math.toRadians(dest.getX() - this.getX());
        double distLatitud = Math.toRadians(dest.getY() - this.getY());

        double a = Math.pow(Math.sin(distLatitud / 2),2) + Math.cos(Math.toRadians(this.getY()))
                * Math.cos(Math.toRadians(dest.getY())) * Math.pow(Math.sin(distLongitud / 2),2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        distance = radiusEarth * c;

        return distance;
    }
}
