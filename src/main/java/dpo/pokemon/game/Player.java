package dpo.pokemon.game;

import dpo.pokemon.structure.Webservice;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Player {
    private String name;
    private int coins;
    private ArrayList<Ball> ballsOwned;
    private ArrayList<Pokemon> captured;
    private Game game;
    private static final int NUM_INIT_BALLS = 3;

    /**
     * Constructor del jugador
     * @param name el nom del jugador
     */
    public Player(String name) {
        this.name = name;
        coins = 1000;
        captured = new ArrayList<>();
        ballsOwned = new ArrayList<>();
    }

    // Getters and setters
    /**
     * @return el nombre de monedes que té el jugador
     */
    public int getCoins() {
        return coins;
    }

    /**
     * @return el nom del jugador
     */
    public String getName() {
        return name;
    }

    /**
     * @return array de Pokémons capturats
     */
    public ArrayList<Pokemon> getCaptured() {
        return captured;
    }

    /**
     * @param name el nom del jugador
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Canvia per les noves monedes del jugador.
     * @param coins les noves monedes del jugador
     */
    public void setCoins(int coins) {
        this.coins = coins;
    }

    /**
     * Llista de les boles que conté
     * @return les boles que té
     */
    public ArrayList<Ball> getBallsOwned() {
        return ballsOwned;
    }

    /**
     * Canvia per la nova llista de boles que té
     * @param ballsOwned nova llista de boles que té
     */
    public void setBallsOwned(ArrayList<Ball> ballsOwned) {
        this.ballsOwned = ballsOwned;
    }

    /**
     * Canvia la llista de captures
     * @param captured nova llista de captures
     */
    public void setCaptured(ArrayList<Pokemon> captured) {
        this.captured = captured;
    }

    /**
     *
     * @return retorna el partit
     */
    public Game getGame() {
        return game;
    }

    /**
     * Canvia el partit
     * @param game el partit
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * Inicialitza la partida del jugador.
     * @param game: La partida inicialitzada que s’assigna al jugador.
     */
    public void initPlayerGame(Game game){
        this.game = game;
        // Al principi, el jugador tindrà 3 Pokéballs
        Ball[] gameBalls = game.getPokeballs();
        for(int i = 0; i < gameBalls.length; i++) {
            if (gameBalls[i].getName().equals("pokeball")) {
                // Afegim 3 Pokéballs
                for(int j = 0; j < NUM_INIT_BALLS; j++) {
                    ballsOwned.add(gameBalls[i]);
                }
                break;
            }
        }
    }

    /**
     * Mostra l'estat del jugador: les seves monedes, els Pokéballs i el nombre de Pokémons capturats
     */
    public void showStats() {
        System.out.println("\nL'estat del jugador");
        System.out.println("Monedes: " + coins);
        System.out.println("Balls:");
        Ball[] gameBalls = game.getPokeballs();
        for(int i = 0; i < gameBalls.length; i++) {
            System.out.println("\t" + firstLetterToUpperCase(gameBalls[i].getName()) + ": " + countTotalBalls(gameBalls[i].getName()));
        }

        System.out.println("Pokemons capturats: " + captured.size());
        System.out.println(" ");
    }

    /**
     * Calcula el nombre de Pokéballs del tipus especificat que té el jugador.
     * @param name: el nom del Pokéball.
     * @return Retorna el nombre de Pokéballs del tipus indicat que té el jugador.
     */
    public int countTotalBalls(String name){
        int numBalls = 0;
        for (int i = 0; i < ballsOwned.size(); i++) {
            Ball ball = ballsOwned.get(i);
            if(ball.getName().equals(name)) {
                numBalls++;
            }
        }
        return numBalls;
    }

    /**
     * Gestiona la compra de monedes
     * @param numMonedes el nombre de monedes que el jugador vol comprar
     */
    public void buyCoins (int numMonedes) {
        Scanner scan = new Scanner(System.in);
        float totalCost = 0, preuPerMoneda;
        String confirm;

        // Calcular el preu per cada móneda en funció de la quantitat
        preuPerMoneda = calculateCoinValue(numMonedes);

        if (preuPerMoneda > 0) {

            // Calcular el preu total
            totalCost = numMonedes * preuPerMoneda / 100;

            System.out.printf("\nEl preu total és de %.2f€. Confirma la compra? (Y/N)\n", totalCost);
            confirm = scan.nextLine();
            if (confirm.equals("Y") || confirm.equals("y")) {
                addCoins(numMonedes);
                System.out.println("\nS'han afegit " + numMonedes + " monedes al seu compte.");
            } else {
                System.out.println("\nCompra cancel·lada.");
            }
        }
    }

    /**
     * Calcula el preu de cada moneda en céntims
     * @param numMonedes el nombre de monedes que vol comprar
     * @return el preu de cada moneda en céntims
     */
    private float calculateCoinValue(int numMonedes) {
        double preuPerMoneda = -1;
        if (!isPositive(numMonedes)) {
            System.out.println("\nCal introduir un nombre estrictament positiu.");
        } else if (numMonedes < 250) {
            preuPerMoneda = 1;
        } else if (numMonedes < 500) {
            preuPerMoneda = 0.9;
        } else if (numMonedes < 1000) {
            preuPerMoneda = 0.7;
        } else if (numMonedes < 10000) {
            preuPerMoneda = 0.5;
        } else {
            preuPerMoneda = 0.25;
        }
        return (float)preuPerMoneda;
    }

    /**
     * Determina si el número és positiu
     * @param number el número a comprovar
     * @return true si és positiu. Altrament, false.
     */
    private boolean isPositive(int number) {
        return number > 0;
    }

    /**
     *  Gestiona la compra de Pokéballs
     */
    public void buyBalls() {
        Ball ball = new Ball();
        Scanner sc = new Scanner(System.in);
        int costTotalBalls = 0;
        int numBalls = 0;

        String ballTypeString = sc.nextLine();
        ball = getBall(ballTypeString);

        // Si la bola no existeix, estarà a null. Llavors, no entra.
        if (ball != null) {
            System.out.println("\nQuantes unitats en vol comprar?");
            numBalls = sc.nextInt();

            // Calcular el cost total de balls segons el tipus
            costTotalBalls = numBalls * ball.getPrice();

            // Comprovar si el nombre de balls és estrictament positiu i si tenim suficient diners
            if (isPositive(numBalls) && enoughCoins(costTotalBalls)) {
                addPokeballs(numBalls, ball, costTotalBalls);
                spendCoins(costTotalBalls);
            } else {
                if (!isPositive(numBalls)) {
                    System.out.println("\nCal introduir un nombre esctrictament positiu.");
                } else {
                    if (!enoughCoins(costTotalBalls)) {
                        System.out.println("\nHo sentim, però no disposa de suficients monedes.");
                    }
                }
            }
        }
        if (ball == null) {
            System.out.println("Opció incorrecte!");
        }
    }

    /**
     * Afegeix boles segons el tipus de Pokéball que ha comprat el jugador.
     * @param numBalls el nombre de Pokéballs a afegir
     * @param ball el Pokéball que ha comprat el jugador
     * @param total el nombre total de monedes que el jugador ha gastat
     */
    private void addPokeballs(int numBalls, Ball ball, int total) {
        for(int i = 0; i < numBalls; i++) {
            this.ballsOwned.add(ball);
        }
        System.out.println("\nS'han afegit " + numBalls + " " + ball.getName() + "s al seu compte a canvi de " + total + " monedes.");
    }

    /**
     * Decrementa el nombre de Pokéballs que el jugador té segons el tipus
     * @param name el nom de la Pokéball
     */
    public void minusPokeballs (String name) {
        if (ballsOwned.size() != 0) {
            for(int i = 0; i < ballsOwned.size(); i++) {
                if (ballsOwned.get(i).getName().equals(name)) {
                    ballsOwned.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * Decrementa el nombre de monedes totals del jugador
     * @param numCoins el nombre total de monedes que el jugador
     *                 ha gastat per comprar objectes.
     */
    private void spendCoins(int numCoins) {
        coins =  getCoins() - numCoins;
    }

    /**
     * Incrementa el nombre de monedes totals del jugador
     * @param numCoins el nombre total de monedes que el jugador ha comprat.
     */
    private void addCoins(int numCoins) {
        coins = getCoins() + numCoins;
    }

    /**
     * Agafa la bola que correspon al nom de la bola especificat.
     * @param type el tipus de bola escollit
     * @return Retorna l’objecte Ball que conté tots els atributs de la bola.
     */
    private Ball getBall(String type) {
        char selected = type.charAt(0);
        int index = selected - 'a';
        if (index >= 0 && index<= game.totalBalls()) {
            Ball[] balls = game.getPokeballs();
            return game.getTheBall(balls[index].getName());
        }
        else {
            return null;
        }

    }

    /**
     * Comprova si el jugador té suficient diners per comprar objectes.
     * @param coins el nombre de monedes que es vol gastar
     * @return true si té diners suficients. Altrament, false.
     */
    private boolean enoughCoins(int coins) {
        return this.coins >= coins;
    }

    /**
     * Mostra la llista d’objectes que té el jugador.
     */
    public void showInventory() {
        int total = 0;
        if (ballsOwned.size() != 0) {
            for(int i = 0; i < game.getPokeballs().length; i++){
                total = countTotalBalls(game.getPokeballs()[i].getName());
                if(total != 0) {
                    System.out.println(total + "x " + firstLetterToUpperCase(game.getPokeballs()[i].getName()));
                }
            }
        }
    }

    /**
     * Afegeix el Pokémon capturat a la llista de Pokémons capturats del jugador.
     * @param pokemon el Pokémon que s'ha d'afegir a la llista de capturats
     * @return sempre retorna true
     */
    public boolean addCapturedPokemon(Pokemon pokemon) {
        captured.add(pokemon);
        System.out.println("\nEl Pokémon " + firstLetterToUpperCase(pokemon.getName()) + " ha estat capturat!");
        return true;
    }

    /**
     * Converteix la primera lletra d’un string en majúscula.
     * @param str el string a tractar
     * @return el string modificat.
     */
    private String firstLetterToUpperCase(String str) {
        String out = str.substring(0,1).toUpperCase() + str.substring(1);
        return out;
    }

    /**
     * Mostra una llista de tots els Pokémons capturats en un HTML.
     * @throws FileNotFoundException
     */
    public void showCaptured() throws FileNotFoundException {
        Webservice ws = new Webservice();
        if (captured.isEmpty()) {
            System.out.println("No ha capturat cap Pokemon.");
        } else {
            System.out.println("Ha capturat " + captured.size() + " Pokémons en total.");
            ws.generateHTMLCaptured(this);
        }
    }

    /**
     * Determina si el jugador disposa del Pokéball del tipus especificat.
     * @param name el tipus de Pokéball
     * @return true si el jugador en té. Altrament, false.
     */
    public boolean hiHaPokeball(String name) {
        int total = countTotalBalls(name);
        if (total != 0){
            return true;
        }
        return false;
    }
}
