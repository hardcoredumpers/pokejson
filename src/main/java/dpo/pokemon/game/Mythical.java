package dpo.pokemon.game;

import com.google.gson.annotations.SerializedName;

public class Mythical extends Pokemon{
    private static String kind = "mythical";
    @SerializedName("special_research")
    private SpecialResearch specialResearch;

    /**
     * @return la Recerca Especial
     */
    public SpecialResearch getSpecialResearch() {
        return specialResearch;
    }

    /**
     * Càlcula el valor de la captura del Pokémon Mític segons de la seva fórmula
     * @param probPokemon la probabilitat de captura del Pokémon
     * @param probBall la probabilitat de captura de la bola
     * @return el valor de la captura calculada
     */
    @Override
    public double calculateCapture(int probPokemon, int probBall) {
        return 1;
    }
}
