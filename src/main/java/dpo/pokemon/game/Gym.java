package dpo.pokemon.game;

public class Gym {
    private String name;
    private Location location;

    /**
     * @return la ubicació del gimnàs
     */
    public Location getLocation () {
        return location;
    }

    /**
     * @return el nom del gimnàs
     */
    public String getName() {
        return name;
    }

    /**
     * @param name el nom del gimnàs
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param location la ubicació del gimnàs
     */
    public void setLocation(Location location) {
        this.location = location;
    }
}
