package dpo.pokemon.game;

public class GenericPokemon extends Pokemon {

    public GenericPokemon() {
    }

    /**
     * Càlcula el valor de la captura del Pokémon Genèric segons de la seva fórmula
     * @param probPokemon la probabilitat de captura del Pokémon
     * @param probBall la probabilitat de captura de la bola
     * @return el valor de la captura calculada
     */
    @Override
    public double calculateCapture(int probPokemon, int probBall) {
        return (((double)probBall/256) + ((double)probPokemon/2048));
    }
}
