package dpo.pokemon.game;
import com.google.gson.annotations.SerializedName;
import dpo.pokemon.pokedex.Sprite;
import dpo.pokemon.structure.JsonReader;
import dpo.pokemon.structure.Webservice;

public abstract class Pokemon {
    @SerializedName("id")
    protected int id;
    @SerializedName("name")
    protected String name;
    @SerializedName("capture_rate")
    protected int captureRate;

    // Atributs que venen de dpo.pokemon.structure.Webservice
    @SerializedName("base_experience")
    protected int baseExperience;
    @SerializedName("height")
    protected int height;
    @SerializedName("weight")
    protected int weight;
    @SerializedName("sprites")
    protected Sprite sprites;

    public Pokemon () {
    }

    /**
     * @return l'identificador del Pokémon
     */
    public int getId() {
        return this.id;
    }

    /**
     * @return el nom del Pokémon
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return la probabilitat de captura del Pokémon
     */
    public int getCaptureRate() {
        return this.captureRate;
    }

    /**
     * @return l'experiència base del Pokémon
     */
    public int getBaseExperience() {
        return baseExperience;
    }

    /**
     * @return l'altura del Pokémon
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return el pes del Pokémon
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @return el Sprites del Pokémon
     */
    public Sprite getSprites() {
        return sprites;
    }

    /**
     * @param id l'identificador del Pokémon
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param name el nom del Pokémon
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param captureRate la probabilitat de captura del Pokémon
     */
    public void setCaptureRate(int captureRate) {
        this.captureRate = captureRate;
    }

    /**
     * @param baseExperience l'experiència base del Pokémon
     */
    public void setBaseExperience(int baseExperience) {
        this.baseExperience = baseExperience;
    }

    /**
     * @param height l'altura del Pokémon
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @param weight el pes del Pokémon
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @param sprites els Sprites del Pokémon
     */
    public void setSprites(Sprite sprites) {
        this.sprites = sprites;
    }

    /**
     * Calcula el valor de la captura del Pokémon entrre 0 i 1
     * @param probPokemon la probabilitat de captura del Pokémon
     * @param probBall la probabilitat de captura de la Pokéball
     * @return el valor de la captura del Pokémon entre 0 i 1
     */
    public abstract double calculateCapture(int probPokemon, int probBall);

    /**
     * Agafa les dades de l'Especie de Pokémon del Webservice
     * @return el Pokémon amb les dades trets del Webservice
     */
    public Pokemon getPokemonSpecieDataFromWs() {
        Webservice ws = new Webservice();
        JsonReader jr = new JsonReader();
        String urlPokemon = "https://pokeapi.co/api/v2/pokemon/" + this.getName() +"/";
        String pokemonData = ws.getJsonString(urlPokemon);
        PokemonAux aux = jr.getPokemonForDetails(pokemonData);
        this.setBaseExperience(aux.getBaseExperience());
        this.setHeight(aux.getHeight());
        this.setWeight(aux.getWeight());
        this.setSprites(aux.getSprites());
        return this;
    }
}