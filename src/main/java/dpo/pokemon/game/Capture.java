package dpo.pokemon.game;

/**
 *
 */
public class Capture {
    private int numTries;
    private Pokemon pokeToBeCaptured;
    Player player;

    /**
     * Constructor de la classe
     * @param pokemon El Pokémon a capturar
     * @param player El jugador qui intenta capturar el Pokémon
     */
    public Capture(Pokemon pokemon, Player player) {
        numTries = 5;
        pokeToBeCaptured = pokemon;
        this.player = player;
    }

    // Getters i setters
    /**
     * @return numTries el nombre d'intents que quedan per fer la captura
     */
    public int getNumTries() {
        return numTries;
    }

    /**
     * @return pokeToBeCaptured el Pokémon a capturar
     */
    public Pokemon getPokeToBeCaptured() {
        return pokeToBeCaptured;
    }

    /**
     * @return player el jugador que intenta capturar el Pokémon
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @param numTries el nombre d'intents nou que es vol ficar
     */
    public void setNumTries(int numTries) {
        this.numTries = numTries;
    }

    /**
     * @param pokeToBeCaptured el Pokémon que es vol capturar
     */
    public void setPokeToBeCaptured(Pokemon pokeToBeCaptured) {
        this.pokeToBeCaptured = pokeToBeCaptured;
    }

    /**
     * @param player el jugador que intenta capturar el Pokémon
     */
    public void setPlayer(Player player) {
        this.player = player;
    }
}
