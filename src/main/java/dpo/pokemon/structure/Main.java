package dpo.pokemon.structure;

import dpo.pokemon.game.Game;
import dpo.pokemon.game.Player;

import java.util.Scanner;

/**
 * El procediment principal del programa que s'executa al iniciar el joc
 */
public class Main {
    public static void main (String[] args) {
        try {
            // Demanem el nom del jugador
            Scanner sc = new Scanner(System.in);
            System.out.println("Introdueix el nom del jugador: ");
            String playerName = sc.nextLine();

            // Inicialitzem el jugador i comencem la partida
            Player player = new Player(playerName);

            // Inicialitzem la partida
            Game game = new Game(player);
            game.initGame();
            player.initPlayerGame(game);

            Menu m = new Menu();
            String option;
            do {
                do {
                    player.showStats();
                    m.displayMenu();
                    option = m.getOption();

                    if (!m.isCorrect(option)) {
                        m.printErrorOption();
                    }
                } while(!m.isCorrect(option));
                game.letTheGameBegin(option);

            } while (!m.isExit(option));


        } catch (Exception e) {
                System.out.println(e.getMessage());
                System.exit(-1);
        }
        finally {
            System.exit(0);
        }
    }
}
