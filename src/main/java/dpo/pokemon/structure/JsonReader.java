package dpo.pokemon.structure;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.*;
import dpo.pokemon.game.*;
import dpo.pokemon.pokedex.PokemonSpecie;

public class JsonReader {

    private BufferedReader br;
    /**
     * Agafa més informació sobre el Pokémon a partir d'un string
     * @param json el string en format JSON que conté la informació del Pokémon
     * @return el Pokémon auxiliar que conté la informació
     */
    public PokemonAux getPokemonForDetails (String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, PokemonAux.class);
    }

    /**
     * Agafa la informació sobre la espècie de Pokémon a partir d'un string
     * @param json el string en format JSON que con´té la informació de l'especie de Pokémon
     * @return l'Especie de Pokémon que conté la informació
     */
    public PokemonSpecie getPokemonSpecie (String json) {
        Gson gson = new Gson();

        return gson.fromJson(json, PokemonSpecie.class);
    }

    /**
     * Agafa les dades dels Pokémons d'un fitxer poke.json
     * @return array de Pokémons auxiliars amb les dades trets del fitxer
     */
    public PokemonAux[] getPokemonsAux () {
        Gson gson = new Gson();
        try {
            this.br = new BufferedReader(new FileReader("src/main/resources/poke.json"));
            return gson.fromJson(br, PokemonAux[].class);
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Agafa les dades dels tipus de Pokéballs d'un fitxer balls.json
     * @return array de Pokéballs amb la informació tret del fitxer
     */
    public Ball[] getPokeballs () {
        Gson gson = new Gson();
        try {
            this.br = new BufferedReader(new FileReader("src/main/resources/balls.json"));
            return gson.fromJson(br,Ball[].class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Agafa les dades del Pokémon Llegendari d'un fitxer legends.json
     * @return array de Pokémons Llegendaris amb les dades trets del fitxer
     */
    public Legendary[] getLegendary () {
        Gson gson = new Gson();
        try {
            this.br = new BufferedReader(new FileReader("src/main/resources/legends.json"));
            Legendary[] legendaries = gson.fromJson(br, Legendary[].class);
            return getLegendaryAux(legendaries);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Agafa les dades dels Pokémons Mítics d'un fitxer legends.json
     * @return array de Pokémons Mítics amb dades trets del fitxer
     */
    public Mythical[] getMythical () {
        Gson gson = new Gson();
        try {
            this.br = new BufferedReader(new FileReader("src/main/resources/legends.json"));
            Mythical[] mythicals = gson.fromJson(br, Mythical[].class);
            return getMythicalsAux(mythicals);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Filtrar els Pokémons Mitics d'un array
     * @param mitic array que conté tots els Pokémons trets anteriorment
     * @return array de Pokémons de tipus Mític filtrat
     */
    private Mythical[] getMythicalsAux(Mythical[] mitic) {
        ArrayList<Mythical> list = new ArrayList<>();

        for (Mythical value : mitic) {
            if (value.getSpecialResearch() != null) {
                list.add(value);
            }
        }
        Mythical[] aux = new Mythical[list.size()];
        aux = list.toArray(aux);
        return aux;
    }

    /**
     * Filtrar els Pokémons Llegendaris d'un array
     * @param legend array que conté tots els Pokémons trets anteriorment
     * @return array de Pokémons de tipus Llegendari filtrat
     */
    private Legendary[] getLegendaryAux (Legendary[] legend) {
        ArrayList<Legendary> list = new ArrayList<>();

        for (Legendary value : legend) {
            if (value.getGym() != null) {
                list.add(value);
            }
        }

        Legendary[] aux = new Legendary[list.size()];
        aux = list.toArray(aux);
        return aux;
    }
}