package dpo.pokemon.structure;

import dpo.pokemon.game.*;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Menu {
    private String option;
    private static final int FIRST_OPTION = 1;
    private static final int LAST_OPTION = 9;

    /**
     * Comprova si l’opció introduïda per l’usuari és correcte.
     * @param option l’opció introduïda
     * @return true si és correcta. Altrament, false.
     */
    public boolean isCorrect (String option) {
        int aux;
        aux = checkOption(option);

        return aux >= FIRST_OPTION && aux <= LAST_OPTION;
    }

    /**
     * Comprova si l’opció introduïda per l’usuari és per sortir.
     * @param option l’opció introduïda
     * @return true si és per sortir. Altrament, false.
     */
    public boolean isExit (String option) {
        return Integer.parseInt(option) == LAST_OPTION;
    }

    public String getOption () {
        Scanner scan = new Scanner(System.in);
        return option = scan.nextLine();
    }

    /**
     * Mostra el missatge d’error si l’opció és incorrecta.
     */
    public void printErrorOption () {
        System.out.println("ERROR! Introdueix una opció vàlida (1-9)\n");
    }

    public String askUsername () {
        String playerName = "";
        Scanner sc = new Scanner(System.in);
        System.out.println("Introdueix el nom del jugador: ");
        return playerName = sc.nextLine();
    }

    /**
     * Mostra el menú principal del joc
     */
    public void displayMenu () {
        System.out.println("\n1. Afegir Monedes");
        System.out.println("2. Comprar Objectes");
        System.out.println("3. Consultar Inventari");
        System.out.println("4. Buscar Pokémon Salvatge");
        System.out.println("5. Fer Raid");
        System.out.println("6. Recerques Especials Actuals");
        System.out.println("7. Informe de Capturats");
        System.out.println("8. Informació Detallada");
        System.out.println("9. Sortir");
        System.out.println(" ");
        System.out.print("Sel·leccioni una opcio: ");
    }

    /**
     * Comrpova el input que introdueix de l'usuari per determinar si és una opció vàlida
     * @param str el string a comprovar
     * @return el nombre enter que correspon a l'opció
     */
    private int checkString(String str) {
        int i = 0;
        double result = 0;

        // Calcular el valor i convertir l'opció a un enter
        while (str.length() > i && str.charAt(i) >= '0' && str.charAt(i) <= '9') {
            result = result * 10 + (str.charAt(i) - '0');
            i++;
        }
        return (int) result;
    }

    /**
     * Determina quina opció s'ha triat
     * @param str el string que el jugador ha introduït
     * @return el nombre enter que correspon a l'opció
     */
    private int checkOption (String str) {
        StringBuilder stringBuilder = new StringBuilder();
        String asciiStr = null;
        long asciiInt;
        int option = -1;
        for (int i = 0; i < str.length(); i++) {
            stringBuilder.append((int)str.charAt(i));
            char c = str.charAt(i);
        }
        asciiStr = stringBuilder.toString();
        asciiInt = checkString(asciiStr);
        if (asciiInt > 48 && asciiInt < 58) {  option = Character.getNumericValue(Integer.parseInt(asciiStr)); }
        return option;
    }


}
