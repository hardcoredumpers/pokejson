package dpo.pokemon.structure;

import dpo.pokemon.game.Player;
import dpo.pokemon.game.Pokemon;
import dpo.pokemon.game.PokemonAux;
import dpo.pokemon.pokedex.PokemonSpecie;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Webservice {

    /**
     * Agafa el string en format JSON des de PokéAPI amb la URL especificada
     * @param urlString la URL on s'agafen les dades del Pokémon
     * @return el string en format JSON que conté el JSON que ve del Webservice
     */
    public String getJsonString (String urlString) {
        String jsonString = "";

        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode != 200) {
                throw new RuntimeException("ResponseCode: " + responseCode);
            } else {
                jsonString = response.body().string();
            }
        }
        catch (IOException e) {
            if(e.getMessage().contains("handshake_failure")) {
                System.out.println("Webservice (getJsonString) > " + e.getMessage());
            }
        }
        return jsonString;
    }

    /**
     * Genera el fitxer HTML que conté les dades del Pokémon
     * @param pokemon el Pokémon
     * @param pokemonSpecie l'espècie del Pokémon
     * @throws FileNotFoundException
     */
    public void generateHTML (PokemonAux pokemon, PokemonSpecie pokemonSpecie) throws FileNotFoundException {
        String filename = pokemon.getName() + "_info.html";
        File file = new File(filename);
        PrintStream outputFile = new PrintStream(file);
        String pokeName = firstLetterToUpperCase(pokemon.getName());

        // Construim el contingut del fitxer HTML
        String htmlString = "";
        htmlString += "<!doctype html>\n" +
                    "<html lang=\"en\">\n" +
                    "\t<head>\n" +
                    "\t\t<meta charshet=\"UTF-8\">\n" +
                    "\t\t<title>" + pokeName + "</title>\n" +
                    "\t\t<meta name=\"author\" content=\"Nicole Marie Jimenez - nicolemarie.jimenez, Kaye Ann Ignacio - kayeann.ignacio\"></meta>\n" +
                    "\t</head>\n" +
                    "\t<body>\n" +
                    "\t\t<h1>" + pokeName + " (" + pokemon.getId() + ")</h1>\n" +
                    "\t\t<img src=\"" + pokemon.getSprites().getFrontDefault() + "\"></img>\n" +
                    "\t\t<p>" + pokemonSpecie.getFlavorText("en")+ "</p>\n" +
                    "\t\t<li>" + pokemon.getHeight() + " m</li>\n" +
                    "\t\t<li>" + pokemon.getWeight() + " kg</li>\n" +
                    "\t\t<li>" + pokemon.getBaseExperience() + " xp</li>\n" +
                    "\t</body>\n" +
                    "</html>";

        // Escrivim en el fitxer HTML
         outputFile.print(htmlString);

         // Tanquem el fitxer
         outputFile.close();

        System.out.println("Fitxer HTML generat.");
    }

    /**
     * Genera el fitxer HTML dels Pokémons capturats d'un jugador
     * @param player el jugador
     * @throws FileNotFoundException
     */
    public void generateHTMLCaptured (Player player) throws FileNotFoundException {
        int i = 0;
        ArrayList<Pokemon> pokemonsCaptured = player.getCaptured();
        int numCapturats = pokemonsCaptured.size();
        Map<Pokemon, Integer> counts = new HashMap<Pokemon, Integer>();

        // Fem un distinct dels Pokémons, és a dir, per cada Pokémon, tindrem la quantitat
        for (Pokemon poke : pokemonsCaptured) {
            if (counts.containsKey(poke)) {
                counts.put(poke, counts.get(poke) + 1);
            } else {
                counts.put(poke, 1);
            }
        }

        // Creem el fitxer html amb el nom del jugador
        String filename = player.getName() + "_capturedPokemons.html";
        File file = new File(filename);
        PrintStream outputFile = new PrintStream(file);

        // Construim el contingut del fitxer HTML
        String htmlString = "";
        htmlString += "<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "\t<head>\n" +
                "\t\t<meta charshet=\"UTF-8\">\n" +
                "\t\t<title>Pokemon Capturats</title>\n" +
                "\t\t<meta name=\"author\" content=\"Nicole Marie Jimenez - nicolemarie.jimenez, Kaye Ann Ignacio - kayeann.ignacio\"></meta>\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\t<h1>Pokémons capturats: " + numCapturats + "</h1>\n" + "<table>";

        for (Map.Entry<Pokemon, Integer> pokes : counts.entrySet()) {
            Pokemon pokeAux = pokes.getKey().getPokemonSpecieDataFromWs();
            String pokeName = firstLetterToUpperCase(pokes.getKey().getName());
            htmlString += "<tr>\t\t<th><img src=\"" + pokeAux.getSprites().getFrontDefault() + "\"></img></th>\n" +
                    "<th><strong>" + pokeName + "</strong>\nx" + pokes.getValue() + "</th></tr>";
        }
        htmlString += "<table>\t</body>\n" +
                    "</html>";

        // Escrivim el contingut en el fitxer HTML
        outputFile.print(htmlString);

        // Tanquem el fitxer
        outputFile.close();

        System.out.println("\nFitxer HTML generat.");

    }

    /**
     * Converteix la primera lletra d’un string en majúscula.
     * @param str a tractar
     * @return retorna el string modificat
     */
    private String firstLetterToUpperCase(String str) {
        String out = str.substring(0,1).toUpperCase() + str.substring(1);
        return out;
    }
}
